//
//  AppDelegate.swift
//  Social Login
//
//  Created by abhishek on 05/12/17.
//  Copyright © 2017 abhishek. All rights reserved.
//

import UIKit
import FacebookCore
import FBSDKCoreKit
import GoogleSignIn
import Google

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let kClientKey = "1042233110964-csslkf45dt6flflu1917ld6po1cn7g4r.apps.googleusercontent.com"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.initializeFacebook(application, didFinishLaunchingWithOptions: launchOptions)
        self.checkForLogin()
        self.initializeGoogle()
        return true
    }
    func initializeFacebook(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) {
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    func initializeGoogle() {
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(String(describing: configureError))")
        GIDSignIn.sharedInstance().clientID = kClientKey
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let facebookDidHandle = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
        let googleDidHandle = GIDSignIn.sharedInstance().handle(url,
                                                                sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String,
                                                                annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        return facebookDidHandle || googleDidHandle
        }
    
    func checkForLogin() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let navigationController = storyboard.instantiateInitialViewController() as? UINavigationController {
            if FBSDKAccessToken.current() != nil || GIDSignIn.sharedInstance().hasAuthInKeychain() {
               guard
                let vc1 = storyboard.instantiateViewController(withIdentifier: "ViewController") as? ViewController ,
                let vc2 = storyboard.instantiateViewController(withIdentifier: "DetailsViewController") as? DetailsViewController else {return}
                navigationController.viewControllers = [vc1, vc2]
            } else {
                guard let vc1 = storyboard.instantiateViewController(withIdentifier: "ViewController") as? ViewController else {return}
                navigationController.viewControllers = [vc1]
            }
            self.window?.rootViewController = navigationController
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        AppEventsLogger.activate(application)
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
    }
}

