//
//  DetailsViewController.swift
//  Social Login
//
//  Created by abhishek on 05/12/17.
//  Copyright © 2017 abhishek. All rights reserved.
//

import UIKit
import Google
import GoogleSignIn
import FacebookCore
import FacebookLogin
import FBSDKCoreKit
import FBSDKLoginKit

class DetailsViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var userImageView: ImageViewLoader!
    let userDetails = UserDetails.shared
    @IBOutlet weak var specimenTF: UITextField!
    @IBOutlet weak var popUpViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var rightLabel: UILabel!
    @IBOutlet weak var leftLabel: UILabel!
    @IBOutlet weak var labelLeadingConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        nameLabel.text = userDetails.name ?? ""
        emailLabel.text = userDetails.email ?? ""
        if let url = userDetails.url {
            userImageView.downloadedFrom(url: url)
        }
        labelLeadingConstraint.constant = 20
    }
    
    @IBAction func logoutAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        /*if specimenTF.text?.isValidEmail ?? true {
         AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: "Valid Email....")
         } else {
         AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: "InValid Email....")
         }*/
        
        UIView.animate(withDuration: 0.5) {
            self.popUpViewBottomConstraint.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func yesAction(_ sender: UIButton) {
        GIDSignIn.sharedInstance().signOut()
        let loginManager = FBSDKLoginManager()
        loginManager.logOut()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func noAction(_ sender: UIButton) {
        UIView.animate(withDuration: 0.5) {
            self.popUpViewBottomConstraint.constant = -150
            self.view.layoutIfNeeded()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
