//
//  ViewController.swift
//  Social Login
//
//  Created by abhishek on 05/12/17.
//  Copyright © 2017 abhishek. All rights reserved.
//

import UIKit
import Google
import GoogleSignIn
import FacebookCore
import FacebookLogin
import FBSDKCoreKit
import FBSDKLoginKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func facebookLoginAction(_ sender: UIButton) {
        let fbLoginManager: FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) -> Void in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                // if user cancel the login
                if (result?.isCancelled)!{
                    return
                }
                if(fbloginresult.grantedPermissions.contains("email")) {
                    self.getFBUserData()
                }
            }
        }
    }
    
    func getFBUserData() {
        FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
            DispatchQueue.main.async {
                if (error == nil) {
                    //everything works print the user data
                    guard let response = result as? [String: Any] else {return}
                    let userObject = UserDetails.shared
                    userObject.name = response["first_name"] as? String ?? ""
                    userObject.email = response["email"] as? String ?? ""
                    if let picture = response["picture"] as? [String: Any] {
                        userObject.url = self.getImageURL(imageData: picture)
                    }
                }
                self.navigateToDetailView()
            }
        })
    }
    
    func getImageURL(imageData: [String: Any]) -> URL? {
        guard let data = imageData["data"] as? [String: Any] else { return nil }
        let urlString = data["url"] as? String ?? ""
        if let url = URL(string: urlString) {
            return url
        } else {
            return nil
        }
    }
    
    @IBAction func googleLoginAction(_ sender: UIButton) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    func navigateToDetailView() {
        if let contoller = self.storyboard?.instantiateViewController(withIdentifier: "DetailsViewController") as? DetailsViewController {
            self.navigationController?.pushViewController(contoller, animated: true)
        }
    }
}

// MARK: - GoogleLogin Delegates -

extension ViewController: GIDSignInUIDelegate, GIDSignInDelegate {
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        
    }
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            let details = UserDetails.shared
            // Perform any operations on signed in user here.
            let fullName = user.profile.name
            details.name = fullName ?? ""
            let email = user.profile.email
            details.email = email ?? ""
            let imageUrl = user.profile.imageURL(withDimension: 100)
            details.url = imageUrl
            // ...
            self.navigateToDetailView()
        } else {
            print("\(error.localizedDescription)")
        }
    }
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
    }
}
