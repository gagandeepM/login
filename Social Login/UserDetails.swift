//
//  UserDertails.swift
//  Social Login
//
//  Created by abhishek on 05/12/17.
//  Copyright © 2017 abhishek. All rights reserved.
//

import UIKit

class UserDetails {
    static let shared = UserDetails()
    var name: String?
    var email: String?
    var url: URL?
}

class AlertMessage {
    
    private static var alertMessageController: UIAlertController!
    
    internal static func displayAlertMessage(titleMessage:String, alertMsg:String) {
        AlertMessage.alertMessageController = UIAlertController(title: titleMessage, message:
            alertMsg, preferredStyle: UIAlertControllerStyle.alert)
        AlertMessage.alertMessageController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default,handler: nil))
        if let controller = UIApplication.shared.keyWindow?.rootViewController?.presentedViewController {
            controller.present(AlertMessage.alertMessageController, animated: true, completion: nil)
        }
        else{
            UIApplication.shared.delegate?.window??.rootViewController?.present(AlertMessage.alertMessageController, animated: true, completion: nil)
        }
        return
    }
}
